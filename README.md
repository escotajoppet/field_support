# README

Reporting
1)      Extract report for the list of Products in each Channel & Store.
Purpose for have a quick/reporting overview of X product carrying at which store.
2)      Report for Staff Visitation Report – by Merchandiser/Team.  This report will  to be extracted based on per person with the list of all channels & outlets visitation. 
3)      Missed outlet report.  This report is for manager use only. This report functionality will indicate the Merchandiser the missed outlets based on their Scheduled stores assigned.
4)     Product Report. Admin will have the capability to extract a report of the product availability.
This is for consolidating report for all the channels/outlets for the review, update and quick on-hand information of the product where it is available. Also will be used to maintain the product listed in each outlet/channel.
5)      Add Outlet/Channel Reporting. Capability to export list of Outlets / Channel for maintenance. With the total numbers of outlets in  the results. This report only extract the full list of Outlets for each channel.

6)      Report for Brands - Products.  Capability to extract the number of listed products for a particular Brand in every store. This is for maintenance usage and for have a quick overview of the existing products in FSS.

7)      Report to export the list of Outlets with Assigned Merchandisers.  It will include the total numbers of outlets that had been assigned and number of outlets not assigned based on the results. This is for maintenance purpose and to avoid outlets not assigned to anyone.

* Ruby version
ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]