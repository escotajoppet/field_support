Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'dashboards#index'

  devise_for :users, :controllers => {
  	:sessions => 'users/sessions'
  }

  resources :users, :except => [:show] do
    collection do
      post  'create_new' => 'users#create'
    end
  end

  resources :dashboards, :only => [:index] do
  	collection do
      get   'products/extractions/channels' => 'products#extractions_channels'
      get   'products'
      get   'staffs' => 'users#staffs'
      get   'channels' => 'channels#reports'
      get   'brands' => 'brands#reports'
    end

    member do
      get   'extractions/stores' => 'products#extractions_stores'
      get   'extractions' => 'products#extractions'

      get   'products/stores' => 'products#stores'
      get   'products/reports' => 'products#reports'
      get   'brand/products' => 'products#brand_reports'

      get   'channels/stores' => 'stores#reports'
    end
  end

  resources :teams, :except => [:show] do
    member do
      get     'accounts'
      post    'add-account-to' => 'teams#add_account_to'
      delete  'remove-account-from' => 'teams#remove_account_from'
    end    
  end

  resources :channels, :except => [:show] do
    member do
      get     'stores'
      post    'add-store-to' => 'channels#add_store_to'
      delete  'remove-store-from' => 'channels#remove_store_from'
    end
  end

  resources :stores, :except => [:show] do
    member do
      get     'products'
      post    'add-product-to' => 'stores#add_product_to'
      delete  'remove-product-from' => 'stores#remove_product_from'
      
      get     'teams'
      post    'add-team-to' => 'stores#add_team_to'
      delete  'remove-team-from' => 'stores#remove_team_from'
    end
  end

  resources :brands, :except => [:show] do
    member do
      get     'products'
      post    'add-product-to' => 'brands#add_product_to'
      delete  'remove-product-from' => 'brands#remove_product_from'
    end
  end

  resources :products, :except => [:show] do
    member do
      post    'upload-report-to' => 'products#upload_report_to'
      post    'report/image/download' => 'products#download_report_image'
      post    'report/date-range' => 'products#date_range'
    end
  end

  resources :reports, :only => [:index] do
    collection do
      post    'generate'
    end

    member do
      get     'products'
    end
  end
end
