require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FieldSupport
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.assets.enabled = true
    
		config.assets.paths << Rails.root.join('vendor', 'assets', 'components', 'font-awesome')
		config.assets.paths << Rails.root.join('vendor', 'assets', 'components', 'ionicons')
		config.assets.paths << Rails.root.join('vendor', 'assets', 'components', 'admin-lte')
		
		config.to_prepare do
			Devise::SessionsController.layout 'devise'
			Devise::RegistrationsController.layout 'application'
		end

		config.autoload_paths << Rails.root.join('app', 'pdfs')
  end
end
