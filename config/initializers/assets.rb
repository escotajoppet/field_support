# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

precompiled_assets = {
	'field_support' => [],
	'dashboards' => ['products'],
	'users' => ['index', 'new', 'create', 'edit', 'update'],
	'teams' => ['index', 'new', 'create', 'edit', 'update', 'accounts'],
	'channels' => ['index', 'new', 'create', 'edit', 'update', 'stores'],
	'stores' => ['index', 'new', 'create', 'edit', 'update', 'products', 'teams'],
	'brands' => ['index', 'new', 'create', 'edit', 'update', 'products'],
	'products' => ['index', 'new', 'create', 'edit', 'update', 'extractions_channels', 'extractions_stores', 'extractions', 'stores', 'reports'],
	'reports' => ['index'],
	'plugins' => ['chart.bundle.min', 'moment.min']
}

precompiled_assets.each do |controller, actions|
	Rails.application.config.assets.precompile += ["#{controller}.css", "#{controller}.js"]

	`touch #{Rails.root.join('app', 'assets', 'javascripts', "#{controller}.js")} || exit`
	`touch #{Rails.root.join('app', 'assets', 'stylesheets', "#{controller}.css")} || exit`

	actions.each do |action|
		`mkdir -p #{Rails.root.join('app', 'assets', 'javascripts', controller)}`
		`touch #{Rails.root.join('app', 'assets', 'javascripts', controller, "#{action}.js")} || exit`

		`mkdir -p #{Rails.root.join('app', 'assets', 'stylesheets', controller)}`
		`touch #{Rails.root.join('app', 'assets', 'stylesheets', controller, "#{action}.css")} || exit`

		Rails.application.config.assets.precompile += ["#{controller}/#{action}.css", "#{controller}/#{action}.js"]
	end
end