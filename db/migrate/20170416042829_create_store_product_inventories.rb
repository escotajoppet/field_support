class CreateStoreProductInventories < ActiveRecord::Migration[5.0]
  def up
    create_table :store_product_inventories, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
      t.string :store_id
      t.string :product_id
      t.integer :inventory_level

      t.timestamps
    end

    add_index :store_product_inventories, :id
    add_index :store_product_inventories, :store_id
    add_index :store_product_inventories, :product_id
  end

  def down
  	drop_table :store_product_inventories
  end
end
