class CreateBrands < ActiveRecord::Migration[5.0]
  def up
    create_table :brands, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
    	t.string :name

      t.timestamps
    end

    add_index :brands, :id

    add_attachment :brands, :avatar
  end

  def down
  	drop_table :brands
  end
end
