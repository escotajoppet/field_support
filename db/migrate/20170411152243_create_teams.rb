class CreateTeams < ActiveRecord::Migration[5.0]
  def up
    create_table :teams, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
    	t.string :name
    	t.string :store_id

      t.timestamps
    end

    add_index :teams, :id
    add_index :teams, :store_id
  end

  def down
  	drop_table :teams
 	end
end
