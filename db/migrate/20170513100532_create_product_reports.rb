class CreateProductReports < ActiveRecord::Migration[5.0]
  def up
    create_table :product_reports, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
      t.string :product_id
      t.string :store_id
      t.integer	:inventory_level
      t.string :staff_id
      t.text :remarks

      t.timestamps
    end

    add_index :product_reports, :product_id
    add_index :product_reports, :store_id
    add_index :product_reports, :staff_id

    add_attachment :product_reports, :avatar
  end

  def down
  	drop_table :product_reports
  end
end
