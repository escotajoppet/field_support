class CreateChannels < ActiveRecord::Migration[5.0]
  def up
    create_table :channels, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
    	t.string :name

      t.timestamps
    end

    add_index :channels, :id

    add_attachment :channels, :avatar
  end

  def down
  	drop_table :channels
  end
end
