class CreateProducts < ActiveRecord::Migration[5.0]
  def up
    create_table :products, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
    	t.string :name
      t.string :brand_id

      t.timestamps
    end

    add_index :products, :id
    add_index :products, :brand_id

    add_attachment :products, :avatar
  end

  def down
  	drop_table :products
  end
end
