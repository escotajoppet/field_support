class CreateStores < ActiveRecord::Migration[5.0]
  def up
    create_table :stores, :id => false do |t|
      t.string :id, :primary_key => true, :null => false, :unique => true
    	t.string :name
      t.string :location
      t.string :channel_id

      t.timestamps
    end

    add_index :stores, :id
    add_index :stores, :channel_id

    add_attachment :stores, :avatar
  end

  def down
  	drop_table :stores
  end
end
