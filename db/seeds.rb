# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create({
	:name => 'Lyn Santos',
	:username => 'lynsantos',
	:password => 'P@55w0rd',
	:password_confirmation => 'P@55w0rd',
	:email => 'lynsantos@field-support.com',
	:user_type => User.user_types[:root_admin]
})

admins = [
	{:name => 'Bradley Kurt', :username => 'bradleykurt'},
	{:name => 'Admin2', :username => 'admin2'},
	{:name => 'Admin3', :username => 'admin3'},
	{:name => 'Admin4', :username => 'admin4'},
	{:name => 'Admin5', :username => 'admin5'},
	{:name => 'Admin6', :username => 'admin6'}
]

admins.each do |admin|
	User.create({
		:name => admin[:name],
		:username => admin[:username],
		:email => "#{admin[:username]}@field-support.com",
		:password => 'P@55w0rd',
		:password_confirmation => 'P@55w0rd',
		:user_type => User.user_types[:admin]
	})
end

staffs = [
	{:name => 'Joppet Escota', :username => 'escotajoppet'},
	{:name => 'Staff2', :username => 'staff2'},
	{:name => 'Staff3', :username => 'staff3'},
	{:name => 'Staff4', :username => 'staff4'},
	{:name => 'Staff5', :username => 'staff5'},
	{:name => 'Staff6', :username => 'staff6'}
]

staffs.each do |staff|
	User.create({
		:name => staff[:name],
		:username => staff[:username],
		:email => "#{staff[:username]}@field-support.com",
		:password => 'P@55w0rd',
		:password_confirmation => 'P@55w0rd',
		:user_type => User.user_types[:staff]
	})
end

("a".."z").each do |team|
	Team.create({ :name => team.upcase })
end

channels = [
	'SM',
	'Robinsons',
	'Ayala Mall'
]

channels.each do |channel|
	Channel.create({ :name => channel	})
end

stores = [
	{:name => 'SM Hypermarket', :location => 'Tondo'},
	{:name => 'SM Save More', :location => 'Nagtahan Bridge'},
	{:name => 'SM Supermarket', :location => 'Sta. Mesa'},
	{:name => 'SM Department Store', :location => 'Cubao'},
	{:name => 'Robinsons Supermarket', :location => 'Tutuban'},
	{:name => 'Robinsons Department Store', :location => 'Ermita'}
]

stores.each do |store|
	Store.create({ 
		:name => store[:name],
		:location => store[:location]
	})
end

brands = [
	'Magnolia',
	'Selecta',
	'Unilever',
	'Maggi',
	'Samsung',
	'LG',
	'Acer',
	'Dell',
	'Lenovo'
]

brands.each do |brand|
	Brand.create({ :name => brand })
end

products = [
	'Cornetto',
	'3 in 1 + 1',
	'Supreme',
	'Temptations',
	'Sorbetes'
]

products.each do |product|
	Product.create({ :name => product })
end