function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('reports');

		setInventoryLevels();
		setChannelsAndStores();
		setICheck();
		// generateReports();
	}
}

function setInventoryLevels(){
	$('#inventory-level').click(function(){
		$('.filters').append(inventory_levels_template);

		setICheck();

		$('#remove-il-filter').click(function(){
			$('#inventory-level-div').remove();
			$('#inventory-level').css('display', '')

			checkFilterBtn();
		});

		$(this).css('display', 'none');
		checkFilterBtn();
	});
}

function setChannelsAndStores(){
	$('#channels-and-stores').click(function(){
		setChannels($('#productcode').val(), true);

		$('#productcode').change(function(){
			setChannels($(this).val(), false);
		});

		$(this).css('display', 'none');

		checkFilterBtn();
	});
}

function setChannels(productcode, newCas){
	$.ajax({
		url: channels_path,
		dataType: 'JSON',
		data: { productcode: productcode },
		success: function(data){
			cas = "<div class=\"form-group col-md-4\" id=\"channels-and-stores-div\">";
			cas += 	"<div class=\"row\">";
			cas += 		"<div class=\"col-md-12\">";
			cas += 			"<label class=\"pull-left\">Channels</label>";
			cas += 			"<a href=\"#\" class=\"pull-right\" id=\"remove-cas-filter\">";
			cas += 				"<i class=\"fa fa-remove\"></i>";
			cas += 			"</a>";
			cas += 		"</div>";
			cas += 	"</div>";

			cas +=	"<select name=\"channelcode\" id=\"channels-select\" class=\"form-control\">";
			cas +=		"<option value=\"\">Select a channel...</option>";

			for(var i = 0; i < data.length; i++) cas +=	"<option value=\""+ data[i].code +"\">"+ data[i].name +"</option>";

			cas +=	"</select>";

			cas += 	"<div class=\"row\">";
			cas += 		"<div class=\"col-md-12\" id=\"stores-div\"></div>";
			cas += 	"<div>";
			cas += "</div>";

			$('#channels-and-stores-div').remove();
			$('.filters').append(cas);

			$('#remove-cas-filter').click(function(){
				$('#channels-and-stores-div').remove();
				$('#channels-and-stores').css('display', '')
				checkFilterBtn();
			});

			$('#channels-select').change(function(){
				$.ajax({
					url: stores_path,
					dataType: 'JSON',
					data: { 
						channelcode: $(this).val(), 
						productcode: $('#productcode').val() 
					},
					success: function(data){
						checkboxes =	"<div class=\"row\">";
						checkboxes +=		"<div class=\"col-md-12\">";
						checkboxes +=			"<label>Stores</label>";
						checkboxes +=		"</div>";					
						checkboxes +=	"</div>"

						checkboxes +=	"<div class=\"row\">";

						for(var i = 0; i < data.length; i++){
							checkboxes += 	"<div class=\"col-md-12\">";
							checkboxes +=		"<input type=\"checkbox\" name=\"stores[]\" value=\""+ data[i].code +"\" class=\"form-control\"/> " + data[i].name;
							checkboxes +=	"</div>"
						}

						checkboxes += "</div>";

						$('#stores-div').html(checkboxes);

						setICheck();
					}
				});
			});
		}
	});
}

function checkFilterBtn(){
	itm_cnt = 0;

	$('#filter-dropdown .dropdown-menu li').each(function(){
		if($(this).css('display') != 'none') itm_cnt += 1
	});

	if(itm_cnt < 1) $('#filter-dropdown button').attr("disabled", "disabled");
	else $('#filter-dropdown button').removeAttr("disabled");
}

function setICheck(){
	$('input[type=checkbox]').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue'
  });
}

function generateReports(){
	$('#generate-reports').click(function(){
		form = $('form');

		$.ajax({
			url: form.attr('action'),
			data: form.serialize(),
			method: 'GET',
			dataType: 'PDF',
			success: function(data){
				var blob=new Blob([data]);
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        link.download="Report_"+new Date()+".pdf";
        link.click();
			}
		})

		return false;
	});
}

whenJQueryIsReady();