function newCreate(){
	setSaveTeamOnClick();
}

function editUpdate(){
	setSaveTeamOnClick();
}

function setSaveTeamOnClick(){
	$('#link-save-team').click(function(e){
		e.preventDefault();

		$('#form-teams').submit();
	});
}