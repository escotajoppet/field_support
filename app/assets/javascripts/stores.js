function newCreate(){
	setAvatarOnClick();
	setSaveStoreOnClick();
}

function editUpdate(){
	setAvatarOnClick();
	setSaveStoreOnClick();
}

function setAvatarOnClick(){
	$('#link-change-avatar').click(function(){
		$('#file-field-change-avatar').trigger('click');

		return false;
	});
}

function setSaveStoreOnClick(){
	$('#link-save-store').click(function(e){
		e.preventDefault();

		$('#form-stores').submit();
	});
}