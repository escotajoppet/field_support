function newCreate(){
	setAvatarOnClick();
	setSaveUserOnClick();
}

function editUpdate(){
	setAvatarOnClick();
	setSaveUserOnClick();
}

function setAvatarOnClick(){
	$('#link-change-avatar').click(function(){
		$('#file-field-change-avatar').trigger('click');

		return false;
	});
}

function setSaveUserOnClick(){
	$('#link-save-user').click(function(e){
		e.preventDefault();

		$('#form-users').submit();
	});
}

function activateAccountsSideNav(){
	if(account_type === admin) activateSideNav('administrators');
	else activateSideNav('staffs');
}