function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		$(".flash").delay(5000).fadeOut();
	}
}

function indexPage(){
	$('tbody > tr').click( function() {
		window.location = $(this).attr('data-href');
	}).hover( function() {
	    $(this).toggleClass('hover');
	});
}

function activateSideNav(name, has_sub=false){
	$("#"+ name +"-side-nav").addClass('active');

	if(has_sub) $("#"+ name +"-side-nav ul").addClass('menu-open');
}

whenJQueryIsReady();