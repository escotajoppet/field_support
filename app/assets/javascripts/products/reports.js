function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		if(view == 'graph')
			setChart();

		setDateRangePicker();
	}
}

function setChart(){
	new Chart($('#myChart'), {
    type: 'bar',
    data: {
      labels: labels,
      datasets: [{
        label: 'Inventory Level',
        data: data,
        backgroundColor: background_colors,
        borderWidth: 2
      }]
    },
    options: {
      scales: {
        yAxes: [{
          display: false
        }]
      }
    }
	});
}

function setDateRangePicker(){
	var start = moment().subtract(30, 'days');
  var end = moment();

  if(sdate !== '' && edate !== ''){
		start = sdate;
		end = edate;
	}

	options = {
		locale: {
			format: 'YYYY-MM-DD'
		},

		startDate: start,
		endDate: end
	};

	$('#report-date-range').daterangepicker(options);
}

whenJQueryIsReady();