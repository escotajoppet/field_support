function newCreate(){
	setAvatarOnClick();
	setSaveProductOnClick();
}

function editUpdate(){
	setAvatarOnClick();
	setSaveProductOnClick();
}

function setAvatarOnClick(){
	$('#link-change-avatar').click(function(){
		$('#file-field-change-avatar').trigger('click');

		return false;
	});
}

function setSaveProductOnClick(){
	$('#link-save-product').click(function(e){
		e.preventDefault();

		$('#form-products').submit();
	});
}