function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('channels');
		addStoreToChannel();
		removeStoreFromChannel();
	}
}

function addStoreToChannel(){
	$('.btn-add-to-channel').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: add_store_to_channel_path,
			method: 'POST',
			dataType: 'script',
			data: {'store_id': id},
			success: function(){
				rebindAddAndRemoveStoreOnClick()
			}
		});
	});
}

function removeStoreFromChannel(){
	$('.btn-remove-from-channel').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: remove_store_from_channel_path,
			method: 'DELETE',
			dataType: 'script',
			data: {'store_id': id},
			success: function(){
				rebindAddAndRemoveStoreOnClick()
			}
		});
	});
}

function rebindAddAndRemoveStoreOnClick(){
	$('.btn-add-to-store').off('click');
	addStoreToChannel();
	$('.btn-remove-from-store').off('click');
	removeStoreFromChannel();		
}

whenJQueryIsReady();