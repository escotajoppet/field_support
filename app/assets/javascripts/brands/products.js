function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('brands');
		addProductToBrand();
		removeProductFromBrand();
	}
}

function addProductToBrand(){
	$('.btn-add-to-brand').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: add_product_to_brand_path,
			method: 'POST',
			dataType: 'script',
			data: {'product_id': id},
			success: function(){
				rebindAddAndRemoveProductOnClick()
			}
		});
	});
}

function removeProductFromBrand(){
	$('.btn-remove-from-brand').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: remove_product_from_brand_path,
			method: 'DELETE',
			dataType: 'script',
			data: {'product_id': id},
			success: function(){
				rebindAddAndRemoveProductOnClick()
			}
		});
	});
}

function rebindAddAndRemoveProductOnClick(){
	$('.btn-add-to-brand').off('click');
	addProductToBrand();
	$('.btn-remove-from-brand').off('click');
	removeProductFromBrand();		
}

whenJQueryIsReady();