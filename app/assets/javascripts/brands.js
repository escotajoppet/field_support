function newCreate(){
	setAvatarOnClick();
	setSaveBrandOnClick();
}

function editUpdate(){
	setAvatarOnClick();
	setSaveBrandOnClick();
}

function setAvatarOnClick(){
	$('#link-change-avatar').click(function(){
		$('#file-field-change-avatar').trigger('click');

		return false;
	});
}

function setSaveBrandOnClick(){
	$('#link-save-brand').click(function(e){
		e.preventDefault();

		$('#form-brands').submit();
	});
}