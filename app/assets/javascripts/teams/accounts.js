function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('teams');
		addAccountToTeam();
		removeAccountFromTeam();
	}
}

function addAccountToTeam(){
	$('.btn-add-to-team').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: add_account_to_team_path,
			method: 'POST',
			dataType: 'script',
			data: {'account_id': id},
			success: function(){
				rebindAddAndRemoveAccountOnClick()
			}
		});
	});
}

function removeAccountFromTeam(){
	$('.btn-remove-from-team').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: remove_account_from_team_path,
			method: 'DELETE',
			dataType: 'script',
			data: {'account_id': id},
			success: function(){
				rebindAddAndRemoveAccountOnClick()
			}
		});
	});
}

function rebindAddAndRemoveAccountOnClick(){
	$('.btn-add-to-team').off('click');
	addAccountToTeam();
	$('.btn-remove-from-team').off('click');
	removeAccountFromTeam();		
}

whenJQueryIsReady();