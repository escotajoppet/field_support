function newCreate(){
	setAvatarOnClick();
	setSaveChannelOnClick();
}

function editUpdate(){
	setAvatarOnClick();
	setSaveChannelOnClick();
}

function setAvatarOnClick(){
	$('#link-change-avatar').click(function(){
		$('#file-field-change-avatar').trigger('click');

		return false;
	});
}

function setSaveChannelOnClick(){
	$('#link-save-channel').click(function(e){
		e.preventDefault();

		$('#form-channels').submit();
	});
}