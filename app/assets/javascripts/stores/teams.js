function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('stores');
		addTeamToStore();
		removeTeamFromStore();
	}
}

function addTeamToStore(){
	$('.btn-add-to-store').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: add_team_to_store_path,
			method: 'POST',
			dataType: 'script',
			data: {'team_id': id},
			success: function(){
				rebindAddAndRemoveTeamOnClick()
			}
		});
	});
}

function removeTeamFromStore(){
	$('.btn-remove-from-store').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: remove_team_from_store_path,
			method: 'DELETE',
			dataType: 'script',
			data: {'team_id': id},
			success: function(){
				rebindAddAndRemoveTeamOnClick()
			}
		});
	});
}

function rebindAddAndRemoveTeamOnClick(){
	$('.btn-add-to-store').off('click');
	addTeamToStore();
	$('.btn-remove-from-store').off('click');
	removeTeamFromStore();		
}

whenJQueryIsReady();