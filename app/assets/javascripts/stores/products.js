function whenJQueryIsReady(){
	if(typeof $ === 'undefined'){
		setTimeout('whenJQueryIsReady', 1500);
	} else{
		activateSideNav('stores');
		addProductToStore();
		removeProductFromStore();
	}
}

function addProductToStore(){
	$('.btn-add-to-store').click(function(e){
		var id = $(this).attr('data-id');
		var level = $("#" + id + "-" + "level").val();

		$.ajax({
			url: add_product_to_store_path,
			method: 'POST',
			dataType: 'script',
			data: {'level': level, 'product_id': id},
			success: function(){
				rebindAddAndRemoveProductOnClick()
			}
		});
	});
}

function removeProductFromStore(){
	$('.btn-remove-from-store').click(function(e){
		var id = $(this).attr('data-id');

		$.ajax({
			url: remove_product_from_store_path,
			method: 'DELETE',
			dataType: 'script',
			data: {'spi_id': id},
			success: function(){
				rebindAddAndRemoveProductOnClick()
			}
		});
	});
}

function rebindAddAndRemoveProductOnClick(){
	$('.btn-add-to-store').off('click');
	addProductToStore();
	$('.btn-remove-from-store').off('click');
	removeProductFromStore();		
}

whenJQueryIsReady();