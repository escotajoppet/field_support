class ProductsController < ApplicationController
	before_action { |controller| authenticate_user! unless controller.request.format.json? }
	before_action :products_list, :only => [:index, :reports, :destroy]
	before_action :product_mod, :only => [:edit, :update, :destroy, :reports, :date_range]
	before_action :associations_list, :only => [:new, :create, :edit, :update]
	skip_before_action :verify_authenticity_token, :only => [:upload_report_to, :reports]

	def index
		respond_to do |format|
			format.json { @products = Brand.find(params[:brandcode]).products }
			format.html
		end
	end

	def new
		respond_to { |format| format.html{ @product = Product.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@product = Product.new(product_params)
				@product.brand = Brand.find(params[:brand]) if params[:brand].present?

				if @product.save
					flash[:notice] = I18n.t('field_support.record.saved', :record => @product.name, :action => 'created')
					redirect_to products_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'product', :count => @product.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				@product.brand = nil
				@product.brand = Brand.find(params[:brand]) if params[:brand].present?
				
				if @product.update_attributes(product_params)
					flash[:notice] = I18n.t('field_support.record.saved', :record => @product.name, :action => 'updated')
					redirect_to products_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'product', :count => @product.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@product.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @product.name, :action => 'deleted')

				redirect_to products_path(@paramz)
			end
		end
	end

	def upload_report_to
		respond_to do |format|
			format.json do
				user = User.find_for_database_authentication(:username => params[:username])

				if user.nil?
					render :json => {
						:status => 'AUTHENTICATION_FAILED',
						:message => 'User not found.'
					}
				else
					unless user.staff_user_type?
						render :json => {
							:status => 'AUTHENTICATION_FAILED',
							:message => 'Invalid user type.'
						}
					else
						if user.valid_password?(params[:password])
							product = Product.find(params[:id])
							store = Store.find(params[:store_id])

							report = ProductReport.new
							report.product = product
							report.store = store
							report.staff = user
							report.remarks = params[:remarks]
							report.inventory_level = params[:level].to_i
							report.avatar = params[:image]

							if report.save
								spis = StoreProductInventory.where(:product => product, :store => store)
								spi = spis.first unless spis.empty?
								spi.update_attributes({:inventory_level => report.inventory_level})

								render :json => { :status => 'SUCCESS' }
							else
								render :json => {
									:status => 'UPLOAD_FAILED',
									:message => report.errors.full_messages
								}
							end
						else
							render :json => {
								:status => 'AUTHENTICATION_FAILED',
								:message => 'Incorrect password.'
							}
						end
					end
				end
			end
		end
	end

	# DASHBOARDS

	def stores
		respond_to { |format| format.html { @stores = Product.find(params[:id]).stores } }
	end

	def channels
		respond_to do |format|
			format.html do
				channels = Array.new

				Product.find(params[:id]).stores.each { |store| channels << store.channel unless store.channel.nil? }

				@channels = Channel.where(:id => channels.map(&:id))
			end
		end
	end

	def date_range
		respond_to do |format|
			format.html do
				sdate, edate = params[:date_range].split(' - ')
				redirect_to products_reports_dashboard_path(params[:id], :view => params[:view], :sdate => sdate, :edate => edate)
			end
		end
	end

	def reports
		respond_to do |format|
			sdate = Date.parse(30.day.ago.strftime("%Y-%m-%d"))
			sdate = Date.parse(params[:sdate]) if params[:sdate].present?

			edate = Date.today
			edate = Date.parse(params[:edate]) if params[:edate].present?

			format.html { @reports = @product.reports.generated(sdate, edate) }
			format.pdf do
				pdf = ProductReportPdf.new(@product, sdate, edate)
				render_pdf(pdf, @product.name.gsub(' ', '_'))
			end
		end
	end

	def brand_reports
		respond_to do |format|
			@brand = Brand.find(params[:id])
			@products = @brand.products.ordered('name', 'ASC').paginate(:page => @paramz[:page])

			format.html
			format.pdf do
				pdf = BrandReportPdf.new(@brand, @products)
				render_pdf(pdf, @brand.name.gsub(' ', '_'))
			end
		end
	end

	def download_report_image
		respond_to { |format| format.html { send_file ProductReport.find(params[:id]).avatar.path } }
	end

	###

	def extractions
		respond_to do |format|
			format.html do
				if params[:id].start_with?(Channel::PREFIX)
					@back_url = session[:products_extractions_channels]	
					
					spis = Array.new

					Channel.find(params[:id]).stores.each { |store| store.store_product_inventories.each { |spi| spis << spi } }

					@store_product_inventories = StoreProductInventory.where(:id => spis.map(&:id))
				else
					@back_url = session[:products_extractions_stores]	

					@store_product_inventories = Store.find(params[:id]).store_product_inventories
				end
			end
		end	
	end

	def extractions_stores
		respond_to do |format|
			format.html do
				session[:products_extractions_stores] = request.original_fullpath

				@stores = Channel.find(params[:id]).stores
			end
		end
	end

	def extractions_channels
		respond_to do |format|
			format.html do
				session[:products_extractions_channels] = request.original_fullpath

				@channels = Channel.all
			end
		end
	end

	private

		def product_params
			params.require(:product).permit(:name, :avatar)
		end

		def products_list
			@products = Product.ordered(params[:sortby], params[:order])
			@products = @products.paginate(:page => @paramz[:page])
		end

		def product_mod
			@product = Product.find(params[:id])
		end

		def associations_list
			@brands = Brand.ordered('name', 'ASC')
		end
end
