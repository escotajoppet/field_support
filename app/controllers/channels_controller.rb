class ChannelsController < ApplicationController
	before_action { |controller| authenticate_user! unless controller.request.format.json? }
	before_action :channels_list, :only => [:index, :destroy, :reports]
	before_action :channel_mod, :only => [:edit, :update, :destroy, :stores, :add_store_to, :remove_store_from]

	def index
		respond_to do |format| 
			format.json { @channels = Channel.ordered('name', 'ASC').where(:id => Product.find(params[:productcode]).stores.map { |store| store.channel.id }) if params[:productcode].present? }
			format.html
		end
	end

	def reports
		respond_to { |format| format.html }
	end

	def new
		respond_to { |format| format.html{ @channel = Channel.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@channel = Channel.new(channel_params)

				if @channel.save
					flash[:notice] = I18n.t('field_support.record.saved', :record => @channel.name, :action => 'created')
					redirect_to channels_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'channel', :count => @channel.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				if @channel.update_attributes(channel_params)
					flash[:notice] = I18n.t('field_support.record.saved', :record => @channel.name, :action => 'updated')
					redirect_to channels_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'channel', :count => @channel.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@channel.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @channel.name, :action => 'deleted')

				redirect_to channels_path(@paramz)
			end
		end
	end

	def stores
		respond_to { |format| format.html{ stores_list } }
	end

	def add_store_to
		respond_to do |format|
			format.js do
				store = Store.find(params[:store_id])

				store.channel = @channel
				store.save

				stores_list
			end
		end
	end

	def remove_store_from
		respond_to do |format|
			format.js do
				store = Store.find(params[:store_id])
				store.channel = nil
				store.save

				stores_list
			end
		end
	end

	private

		def channel_params
			params.require(:channel).permit(:name, :avatar)
		end

		def channels_list
			@channels = Channel.ordered(params[:sortby], params[:order])
			@channels = @channels.paginate(:page => @paramz[:page])
		end

		def channel_mod
			@channel = Channel.find(params[:id])
		end

		def stores_list
			@existing_stores = @channel.stores
			@available_stores = Store.unassigned - @existing_stores
		end
end
