class ReportsController < ApplicationController
	before_action :authenticate_user!
	before_action :product_reports, :only => [:products]

	def index
		respond_to do |format|
			format.html do
				@products = Product.ordered('name', 'ASC')
				@channels = Channel.ordered('name', 'ASC')
			end
		end
	end

	def generate
		respond_to do |format|
			format.pdf do
				product = Product.find(params[:productcode])
				stores = product.stores.ordered('name', 'ASC')
				spis = nil

				if params[:channelcode].present?
					stores = params[:stores].present? && !params[:stores].empty? ? stores.where(:id => params[:stores]) : stores.where(:channel_id => params[:channelcode])
				end

				spis = StoreProductInventory.where(:product => product, :store => stores)
				spis = spis.where(:inventory_level => params[:inventory_levels]) if params[:inventory_levels].present? && !params[:inventory_levels].empty?

				pdf = GeneralReportPdf.new(product, spis)
				render_pdf(pdf, product.name.gsub(' ', '_'))
			end
		end
	end

	def products
		respond_to do |format|
			format.pdf do
				pdf = StaffVisitationsReportPdf.new(@staff, @reports)
				render_pdf(pdf, @staff.name.gsub(' ', '_'))
			end

			format.html
		end
	end

	private

		def product_reports
			@staff = User.find(params[:id])
			@reports = ProductReport.where(:staff => @staff)
		end
end
