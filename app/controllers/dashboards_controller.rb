class DashboardsController < ApplicationController
	before_action :authenticate_user!

	def index
		
	end

	# PRODUCTS REPORT

	def products
		respond_to do |format|
			format.html do
				session[:products] = request.original_fullpath

				@products = Product.all
			end
		end
	end

	###
end
