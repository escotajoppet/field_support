class TeamsController < ApplicationController
	before_action :authenticate_user!
	before_action :teams_list, :only => [:index, :destroy]
	before_action :team_mod, :only => [:edit, :update, :destroy, :accounts, :add_account_to, :remove_account_from]
	before_action :associations_list, :only => [:new, :create, :edit, :update]

	def index
		respond_to { |format| format.html }
	end

	def new
		respond_to { |format| format.html{ @team = Team.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@team = Team.new(team_params)
				@team.store = Store.find(params[:store]) if params[:store].present?

				if @team.save
					flash[:notice] = I18n.t('field_support.record.saved', :record => @team.name, :action => 'created')
					redirect_to teams_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'team', :count => @team.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				@team.store = nil
				@team.store = Store.find(params[:store]) if params[:store].present?
				
				if @team.update_attributes(team_params)
					flash[:notice] = I18n.t('field_support.record.saved', :record => @team.name, :action => 'updated')
					redirect_to teams_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'team', :count => @team.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@team.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @team.name, :action => 'deleted')

				redirect_to teams_path(@paramz)
			end
		end
	end

	def accounts
		respond_to { |format| format.html{ accounts_list } }
	end

	def add_account_to
		respond_to do |format|
			format.js do
				account = User.find(params[:account_id])

				account.team = @team
				account.save

				accounts_list
			end
		end
	end

	def remove_account_from
		respond_to do |format|
			format.js do
				account = User.find(params[:account_id])
				account.team = nil
				account.save

				accounts_list
			end
		end
	end

	private

		def team_params
			params.require(:team).permit(:name)
		end

		def teams_list
			@teams = Team.ordered(params[:sortby], params[:order])
			@teams = @teams.paginate(:page => @paramz[:page])
		end

		def team_mod
			@team = Team.find(params[:id])
		end

		def associations_list
			@stores = Store.ordered('name', 'ASC')
		end

		def accounts_list
			@existing_accounts = @team.users
			@available_accounts = User.staffs.or(User.admins) - @existing_accounts
		end
end
