class UsersController < ApplicationController
	before_action :authenticate_user!
	before_action :users_list, :only => [:index, :destroy]
	before_action :user_mod, :only => [:edit, :update, :destroy]
	before_action :associations_list, :only => [:new, :create, :edit, :update]

	def index
		respond_to { |format| format.html }
	end

	def new
		respond_to { |format| format.html{ @user = User.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@user = User.new(user_params)
				@user.user_type = params[:account_type].to_i
				@user.team = Team.find(params[:team]) if params[:team].present?

				if @user.save
					flash[:notice] = I18n.t('devise.registrations.user_signed_up', :user => @user.name)
					redirect_to users_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'user', :count => @user.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				@user.user_type = params[:account_type].to_i if params[:account_type].present?

				@user.team = nil
				@user.team = Team.find(params[:team]) if params[:team].present?

				@user.skip_password_validation = true if params[:user][:password].blank? && params[:user][:password_confirmation].blank?

				if @user.update_attributes(user_params)
					flash[:notice] = I18n.t('devise.registrations.updated', :user => @user.name)

					if @user.root_admin_user_type? || current_user.id.eql?(params[:id])
						redirect_to root_path
					else
						redirect_to params[:account_type].present? ? users_path(@paramz) : root_path
					end
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'user', :count => @user.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@user.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @user.name, :action => 'deleted')

				redirect_to params[:account_type].present? ? users_path(@paramz) : root_path
			end
		end
	end

	# DASHBOARDS

	def staffs
		respond_to do |format|
			format.html do
				@staffs = User.staffs
				@staffs = @staffs.paginate(:page => @paramz[:page])
			end
		end
	end

	private

		def user_params
			params.require(:user).permit(:name, :username, :email, :password, :password_confirmation, :avatar)
		end

		def users_list
			@users = User.ordered(params[:sortby], 'staff', params[:order])
			@users = User.ordered(params[:sortby], 'admin', params[:order]) if params[:account_type].to_i.eql?(User.user_types[:admin])
			@users = @users.paginate(:page => @paramz[:page])
		end

		def user_mod
			@user = User.find(params[:id])
		end

		def associations_list
			@teams = Team.ordered('name', 'ASC')
			@stores = Store.ordered('name', 'ASC')
		end
end
