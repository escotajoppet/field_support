class BrandsController < ApplicationController
	before_action { |controller| authenticate_user! unless controller.request.format.json? }
	before_action :brands_list, :only => [:index, :reports, :destroy]
	before_action :brand_mod, :only => [:edit, :update, :destroy, :products, :add_product_to, :remove_product_from]

	def index
		respond_to do |format|
			format.html
			format.json
		end
	end

	def reports
		respond_to { |format| format.html }
	end

	def new
		respond_to { |format| format.html{ @brand = Brand.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@brand = Brand.new(brand_params)

				if @brand.save
					flash[:notice] = I18n.t('field_support.record.saved', :record => @brand.name, :action => 'created')
					redirect_to brands_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'brand', :count => @brand.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				if @brand.update_attributes(brand_params)
					flash[:notice] = I18n.t('field_support.record.saved', :record => @brand.name, :action => 'updated')
					redirect_to brands_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'brand', :count => @brand.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@brand.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @brand.name, :action => 'deleted')

				redirect_to brands_path(@paramz)
			end
		end
	end

	def products
		respond_to { |format| format.html{ products_list } }
	end

	def add_product_to
		respond_to do |format|
			format.js do
				product = Product.find(params[:product_id])

				product.brand = @brand
				product.save

				products_list
			end
		end
	end

	def remove_product_from
		respond_to do |format|
			format.js do
				product = Product.find(params[:product_id])
				product.brand = nil
				product.save

				products_list
			end
		end
	end

	private

		def brand_params
			params.require(:brand).permit(:name, :avatar)
		end

		def brands_list
			@brands = Brand.ordered(params[:sortby], params[:order])
			@brands = @brands.paginate(:page => @paramz[:page])
		end

		def brand_mod
			@brand = Brand.find(params[:id])
		end

		def products_list
			@existing_products = @brand.products
			@available_products = Product.all - @existing_products
		end
end
