class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_paramz

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def set_paramz # DRY code for paramz
  	@paramz ||= Hash.new
  	@paramz[:account_type] = params[:account_type] if params[:account_type].present?

    # page params

    @paramz[:page] = 1
    @paramz[:page] = params[:page] if params[:page].present?

    # @paramz[:per_page] = 10
  	# @paramz[:per_page] = params[:per_page] if params[:per_page].present?

    # sorting params

    @paramz[:sortby] = params[:sortby] if params[:sortby].present?
    @paramz[:order] = params[:order] if params[:order].present?
  end

  private

    def render_pdf(pdf, filename)
      send_data pdf.render, :filename => "#{filename}_report_#{Time.now.strftime("%d%m%Y%H%M%S")}.pdf", :type => "application/pdf"
    end
end
