class StoresController < ApplicationController
	before_action { |controller| authenticate_user! unless controller.request.format.json? }
	before_action :stores_list, :only => [:index, :destroy]
	before_action :store_mod, :only => [:edit, :update, :destroy, :products, :add_product_to, :remove_product_from, :teams, :add_team_to, :remove_team_from]
	before_action :associations_list, :only => [:new, :create, :edit, :update]

	def index
		respond_to do |format|
			format.json do
				@stores = Product.find(params[:productcode]).stores.ordered('name', 'ASC') if params[:productcode].present?
				@stores = @stores.where(:channel_id => params[:channelcode]) if params[:channelcode].present?
			end

			format.html
		end
	end

	def reports
		respond_to do |format|
			@channel = Channel.find(params[:id])
			@stores = @channel.stores.ordered('name', 'ASC').paginate(:page => @paramz[:page])
			
			format.html
			format.pdf do
				pdf = ChannelReportPdf.new(@channel, @stores)
				render_pdf(pdf, @channel.name.gsub(' ', '_'))
			end
		end
	end

	def new
		respond_to { |format| format.html{ @store = Store.new } }
	end

	def create
		respond_to do |format|
			format.html do
				@store = Store.new(store_params)
				@store.channel = Channel.find(params[:channel]) if params[:channel].present?

				if @store.save
					flash[:notice] = I18n.t('field_support.record.saved', :record => @store.name, :action => 'created')
					redirect_to stores_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'store', :count => @store.errors.size)
					render :new
				end
			end
		end
	end

	def edit
		respond_to { |format| format.html }
	end

	def update
		respond_to do |format|
			format.html do
				@store.channel = nil
				@store.channel = Channel.find(params[:channel]) if params[:channel].present?
				
				if @store.update_attributes(store_params)
					flash[:notice] = I18n.t('field_support.record.saved', :record => @store.name, :action => 'updated')
					redirect_to stores_path(@paramz)
				else
					flash.now[:alert] = I18n.t('errors.messages.not_saved', :resource => 'store', :count => @store.errors.size)
					render :edit
				end
			end
		end
	end

	def destroy
		respond_to do |format|
			format.html do
				@store.destroy

				flash[:notice] = I18n.t('field_support.record.saved', :record => @store.name, :action => 'deleted')

				redirect_to stores_path(@paramz)
			end
		end
	end

	def teams
		respond_to do |format|
			format.html do
				@existing_teams = @store.teams
				@available_teams = Team.all - @existing_teams
			end
		end
	end

	def add_team_to
		respond_to do |format|
			format.js do
				team = Team.find(params[:team_id])

				team.store = @store
				team.save

				@existing_teams = @store.teams
				@available_teams = Team.all - @existing_teams
			end
		end
	end

	def remove_team_from
		respond_to do |format|
			format.js do
				team = Team.find(params[:team_id])
				team.store = nil
				team.save

				@existing_teams = @store.teams
				@available_teams = Team.all - @existing_teams
			end
		end
	end

	def products
		respond_to { |format| format.html{ products_list } }
	end

	def add_product_to
		respond_to do |format|
			format.js do
				product = Product.find(params[:product_id])

				spi = StoreProductInventory.new
				spi.inventory_level = params[:level].to_i
				spi.product = product
				spi.store = @store

				spi.save

				products_list
			end
		end
	end

	def remove_product_from
		respond_to do |format|
			format.js do
				spi = StoreProductInventory.find(params[:spi_id])
				spi.destroy

				products_list
			end
		end
	end

	private

		def store_params
			params.require(:store).permit(:name, :location, :avatar)
		end

		def stores_list
			@stores = Store.ordered(params[:sortby], params[:order])
			@stores = @stores.paginate(:page => @paramz[:page])
		end

		def store_mod
			@store = Store.find(params[:id])
		end

		def associations_list
			@channels = Channel.ordered('name', 'ASC')
		end

		def products_list
			@existing_store_product_inventories = @store.store_product_inventories
			@existing_products = @store.products
			@available_products = Product.all - @existing_products
		end
end
