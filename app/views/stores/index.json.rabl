collection @stores
attributes :name
node(:code) { |store| store.id }
node(:channel) { |store| store.channel.nil? ? '' : store.channel.name }
