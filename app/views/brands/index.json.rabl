collection @brands
attributes :name
node(:code) { |brand| brand.id }