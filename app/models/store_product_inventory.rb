class StoreProductInventory < ApplicationRecord
	PREFIX = 'SPI'

	belongs_to	:store,
							:class_name => 'Store',
							:foreign_key => 'store_id'

	belongs_to	:product,
							:class_name => 'Product',
							:foreign_key => 'product_id'

	enum :inventory_level => { :high => 0, :medium => 1, :low => 2, :out_of_stock => 3 }, :_suffix => true

	validates_presence_of :inventory_level

	before_create :generate_id

  protected

    def generate_id
      generate_base64_id(self)
    end
end
 