class Channel < ApplicationRecord
  PREFIX = 'CNL'
  
	has_many	:stores,
						:class_name => 'Store',
						:foreign_key => 'channel_id',
            :dependent => :nullify

	ApplicationRecord.attach_file(self)

	validates :name,	:presence => true, 
										:uniqueness => true

	before_create :generate_id

  protected

    def generate_id
      generate_base64_id(self)
    end
end