class Brand < ApplicationRecord
  PREFIX = 'BND'
  
	has_many	:products,
						:class_name => 'Product',
						:foreign_key => 'brand_id',
            :dependent => :nullify

  ApplicationRecord.attach_file(self)

	validates :name, :presence => true,
                   :uniqueness => true

	before_create :generate_id

  protected

    def generate_id
      generate_base64_id(self)
    end
end
