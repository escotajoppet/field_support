class Store < ApplicationRecord
	PREFIX = 'STR'

	has_many	:teams,
						:class_name => 'Team',
						:foreign_key => 'store_id',
						:dependent => :nullify

	has_many	:store_product_inventories,
						:class_name => 'StoreProductInventory',
						:foreign_key => 'store_id',
						:dependent => :delete_all

	has_many	:product_reports,
						:class_name => 'ProductReport',
						:foreign_key => 'store_id',
						:dependent => :nullify

	has_many	:products,
						:class_name => 'Product',
						:through => :store_product_inventories

	has_many	:users,
						:class_name => 'User',
						:foreign_key => 'store_id',
						:dependent => :nullify

	belongs_to	:channel,
							:class_name => 'Channel',
							:foreign_key => 'channel_id',
							:required => false

  ApplicationRecord.attach_file(self)

	validates :name, :presence => true
	validates :location, :presence => true

	before_create :generate_id

  scope :unassigned, -> { where(:channel_id => nil) }

  def select_display
  	"#{self.name} - #{self.location}"
  end

  protected

    def generate_id
      generate_base64_id(self)
    end
end