class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.attach_file(klazz)
	  klazz.has_attached_file :avatar,
	  :path => ":rails_root/public/system/#{klazz.name.downcase.to_sym}/:id/:style/:filename",
  	:url => "/system/#{klazz.name.downcase.to_sym}/:id/:style/:filename",
	  :styles => { medium: "300x300>", thumb: "100x100>", tiny: "50x50>" }, 
	  :default_url => "/system/images/default/:style/default.png"
	  
	  klazz.validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  end

  def self.field_ordered(*args)
    field = args.first.eql?('code') ? 'id' : args.first
    all.order("#{field} #{args.last}")
  end

  def self.assoc_ordered(*args)
    all.includes(args.first.to_sym).order("#{args.first.pluralize}.name #{args.last}")
  end

  def self.ordered(*args)
    if is_field_ordered?(args)
      self.field_ordered(*args)
    else
      self.assoc_ordered(*args)
    end
  end

  def generate_base64_id(obj)
    klazz = obj.class

    obj.id = loop do
      random_id = "#{klazz::PREFIX}#{SecureRandom.hex(8)}".upcase
      break random_id unless klazz.exists?(:id => random_id)
    end
  end

  private

    def self.is_field_ordered?(args)
      self.column_names.include?(args.first) || args.first.nil? || args.last.nil? || args.first.eql?('code')
    end

  WillPaginate.per_page = 10
end