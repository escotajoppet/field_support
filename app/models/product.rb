class Product < ApplicationRecord
	PREFIX = 'PDT'
  
	has_many	:store_product_inventories,
						:class_name => 'StoreProductInventory',
						:foreign_key => 'product_id',
						:dependent => :delete_all

	has_many	:reports,
						:class_name => 'ProductReport',
						:foreign_key => :product_id,
						:dependent => :delete_all

	has_many	:stores,
						:class_name => 'Store',
						:through => :store_product_inventories

	belongs_to	:brand,
							:class_name => 'Brand',
							:foreign_key => 'brand_id',
							:required => false

	delegate :name, :to => :brand, :prefix => true

  ApplicationRecord.attach_file(self)

	validates :name, 	:presence => true, 
										:uniqueness => true

	before_create :generate_id

  protected

    def generate_id
      generate_base64_id(self)
    end
end
