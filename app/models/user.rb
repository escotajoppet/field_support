class User < ApplicationRecord
  attr_accessor :skip_password_validation

  PREFIX = 'USR'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise 	:database_authenticatable, 
          :registerable,
          :recoverable, 
          :rememberable, 
          :trackable, 
          :validatable

  has_many  :reports,
            :class_name => 'ProductReport',
            :foreign_key => 'staff_id'

  belongs_to  :team,
							:class_name => 'Team',
							:foreign_key => 'team_id',
              :required => false

  ApplicationRecord.attach_file(self)

  validates :username, :presence => true, :uniqueness => true
  validates :name, :presence => true

  before_create :generate_id

  enum :user_type => { :root_admin => 0, :admin => 1, :staff => 2 }, :_suffix => true

  scope :staffs, -> { where(:user_type => :staff).order('name ASC') }
  scope :admins, -> { where(:user_type => :admin).order('name ASC') }

  scope :field_ordered, -> (*args) {
    field = args.first.eql?('code') ? 'id' : args.first
    where(:user_type => args[1].to_sym).order("#{field} #{args.last}")
  }

  scope :assoc_ordered, -> (*args){
    where(:user_type => args[1].to_sym).includes(args.first.to_sym).order("#{args.first.pluralize}.name #{args.last}")
  }

  def self.ordered(*args)
    if is_field_ordered?(args)
      self.field_ordered(*args)
    else
      self.assoc_ordered(*args)
    end
  end

  private

    def self.is_field_ordered?(args)
      self.column_names.include?(args.first) || args.first.nil? || args.last.nil? || args.first.eql?('code')
    end

  protected

    def password_required?
      if skip_password_validation
        false
      else
        super
      end
    end

    def generate_id
      generate_base64_id(self)
    end
end
