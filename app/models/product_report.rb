class ProductReport < ApplicationRecord
	PREFIX = 'PRR'

	belongs_to	:product,
							:class_name => 'Product',
							:foreign_key => 'product_id'

	belongs_to	:store,
							:class_name => 'Store',
							:foreign_key => 'store_id'

	belongs_to	:staff,
							:class_name => 'User',
							:foreign_key => 'staff_id'

	

	ApplicationRecord.attach_file(self)

	enum :inventory_level => { :high => 0, :medium => 1, :low => 2, :out_of_stock => 3 }, :_suffix => true

	validates_presence_of :inventory_level
	validates_presence_of :store_id
	validates_presence_of :product_id

	before_create :generate_id

	scope :generated, -> (sdate, edate) { where(:created_at => sdate..edate.end_of_day).order('created_at DESC') }

  protected

    def generate_id
      generate_base64_id(self)
    end
end
