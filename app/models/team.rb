class Team < ApplicationRecord
	PREFIX = 'TM'

	belongs_to	:store,
							:class_name => 'Store',
							:foreign_key => 'store_id',
							:required => false

	has_many	:users,
						:class_name => 'User',
						:foreign_key => 'team_id',
						:dependent => :nullify

	validates :name, :presence => true, :uniqueness => true

	before_create	:generate_id

  protected

    def generate_id
      generate_base64_id(self)
    end
end
