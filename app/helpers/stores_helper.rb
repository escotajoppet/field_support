module StoresHelper
	def inventory_level_select(id)
		high = StoreProductInventory.inventory_levels[:high]
		medium = StoreProductInventory.inventory_levels[:medium]
		low = StoreProductInventory.inventory_levels[:low]
		out_of_stock = StoreProductInventory.inventory_levels[:out_of_stock]

		inv_level = ''

		inv_level +=	"<select name=\"inventory_level\" class=\"form-control\" id=\"#{id}-level\">"
		inv_level +=		"<option value=\"#{high}\" class=\"bg-green\">High</option>"
		inv_level +=		"<option value=\"#{medium}\" class=\"bg-yellow\">Medium</option>"
		inv_level +=		"<option value=\"#{low}\" class=\"bg-orange\">Low</option>"
		inv_level +=		"<option value=\"#{out_of_stock}\" class=\"bg-red\"}>Out of Stock</option>"
		inv_level += 	"</select>"

		return inv_level.html_safe
	end
end
