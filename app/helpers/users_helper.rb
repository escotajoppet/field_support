module UsersHelper
	def box_header(role)
		title = ''

		case role.to_i
			when User.user_types[:root_admin]
				title = 'Root Administrators'
			when User.user_types[:admin]
				title = 'Administrators'
			when User.user_types[:staff]
				title = 'Store Staffs'
		end

		return "#{title} List"
	end

	def is_new_create?
		['new', 'create'].include?(params[:action])
	end

	def is_root_admin?
		User.find(params[:id]).root_admin_user_type? unless is_new_create?
	end

	def cancel_user_form
		if is_root_admin?
			root_path
		else
			users_path(@paramz)
		end
	end
end