module DashboardsHelper
	def products_link(channel)
		prod_cnt = 0

		channel.stores.each { |store| store.store_product_inventories.each { |spi| prod_cnt += 1 } }

		products = "<a href=\"#{extractions_dashboard_path(channel, @paramz)}\" class=\"btn btn-info btn-xs\">#{pluralize(prod_cnt, 'Product')}</a>"

		return products.html_safe
	end

	def store_products_levels(store)
		return get_product_levels(store)
	end

	def channel_stores_products_levels(channel)
		return get_product_levels(channel, true)
	end

	def get_product_levels(record, channel=false)
		high_cnt = 0
		med_cnt = 0
		low_cnt = 0
		oos_cnt = 0

		if channel
			record.stores.each do |store|
				store.store_product_inventories.each do |spi|
					case spi.inventory_level
						when 'high'
							high_cnt += 1
						when 'medium'
							med_cnt += 1
						when 'low'
							low_cnt += 1
						when 'out_of_stock'
							oos_cnt += 1
					end
				end
			end
		else
			record.store_product_inventories.each do |spi|
				case spi.inventory_level
					when 'high'
						high_cnt += 1
					when 'medium'
						med_cnt += 1
					when 'low'
						low_cnt += 1
					when 'out_of_stock'
						oos_cnt += 1
				end
			end
		end

		products = ''
		products += "<span class=\"badge bg-green\">#{high_cnt} High</span>#{indent}"
		products += "<span class=\"badge bg-yellow\">#{med_cnt} Medium</span>#{indent}"
		products += "<span class=\"badge bg-orange\">#{low_cnt} Low</span>#{indent}"
		products += "<span class=\"badge bg-red\">#{oos_cnt} Out of Stock</span>"

		return products.html_safe
	end

	def product_channels(product)
		channels = Array.new

		product.stores.each { |store| channels << store.channel unless store.channel.nil? }

		return channels.empty? ? 0 : Channel.where(:id => channels.map(&:id)).count
	end

	def spi_inventory_level(store_id, product_id)
		level = StoreProductInventory.where(:store_id => store_id, :product_id => product_id).first.inventory_level

		ret = "<span class=\"badge bg-#{level_bg(level)}\">#{level.humanize.titleize}</span>"

		return ret.html_safe
	end
end
