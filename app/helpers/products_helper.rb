module ProductsHelper
	def selected_brand
		if params[:brand_id]
			return params[:brand_id]
		else
			if @product.brand.nil? 
				return nil
			else
				return @product.brand.id
			end
		end
	end

	def report_labels
		return @reports.map{|report| report.created_at.strftime("%m/%d/%Y")}
	end

	def report_data
		return @reports.map do |report|
			case report.inventory_level
				when 'high'
					99
				when 'medium'
					66
				when 'low'
					33
				when 'out_of_stock'
					2
			end
		end
	end

	def report_background_colors
		return @reports.map do |report|
			case report.inventory_level
				when 'high'
					'#00a65a'
				when 'medium'
					'#f39c12'
				when 'low'
					'#ff851b'
				when 'out_of_stock'
					'#dd4b39'
			end
		end
	end
end
