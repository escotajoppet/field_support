require 'yaml'

module ApplicationHelper
  def skin
    return 'skin-red' if current_user.root_admin_user_type?
    return 'skin-yellow' if current_user.admin_user_type?
  end

  def side_nav
    yaml_data = YAML.load_file(Rails.root.join("app", "views", "partials", "yamls", "side_nav.yml"))

    mn = "<ul class=\"sidebar-menu\">"

    yaml_data.each do |data| 
      mn += "<li class=\"header\">#{data["header"].humanize.upcase}</li>"
      data["menu"].each { |data_menu| mn += side_nav_menu(data_menu) }
    end

    mn += "</ul>"

    return mn.html_safe
  end

  def side_nav_menu(data, sub=false)
    mn = "<li id=\"#{data["name"].gsub('_', '-')}-side-nav\" #{"class=\"treeview\"" unless sub}>"
    mn +=   "<a href=\"#{data["link"]}\">"
    mn +=     "<i class=\"fa #{data["icon"]}\"></i> <span>#{data["name"].humanize.titleize}</span>"
    mn +=     "<span class=\"pull-right-container\">"
    mn +=       "<small class=\"label pull-right bg-#{data["label"]}\">#{send("#{data["name"]}_count")}</small>" if data["count"]
    mn +=       "<i class=\"fa #{"fa-angle-left" if data["sub_menu"].present?} pull-right\"></i>"
    mn +=     "</span>"
    mn +=   "</a>"

    if data["sub_menu"].present?
      mn += "<ul class=\"treeview-menu\">"
      data["sub_menu"].each { |sub_data| mn += side_nav_menu(sub_data, true) }
      mn += "</ul>"
    else
      mn += "</li>"
    end

    return mn
  end

  def table_header(field)
    url_params = Array.new
    order = ''

    if request.original_url.include?('?')
      request.original_url.split('?').last.split('&').each do |param|
        k, v = param.split('=')

        if k.eql?('order')
          order = v.eql?('ASC') ? 'DESC' : 'ASC'
        else
          url_params << param unless k.eql?('sortby')
        end
      end
    end

    order = 'ASC' if order.blank?

    url_params << "order=#{order}&sortby=#{field}"

    th = "<a href=\"#{request.path}?#{url_params.join('&')}\">"
    th +=   "#{field.humanize.titleize}"
    th +=   "#{indent}<i class=\"fa fa-sort-#{params[:order].eql?('ASC') ? 'down' : 'up' if params[:order].present?}\"></i>" if !params[:sortby].blank? && params[:sortby].eql?(field)
    th += "</a>"

    return th.html_safe
  end

  def box_border(role)
    case role.to_i
      when User.user_types[:root_admin]
        return 'danger'
      when User.user_types[:admin]
        return 'warning'
      when User.user_types[:staff]
        return 'info'
      else
        case current_user.account_type
          when 'root_admin'
            return 'danger'
          when 'admin'
            return 'info'
        end
    end
  end
  
	def field_support_error_alert_notice!(is_flash, resource=nil)
		flash_alerts = Array.new
    type = 'danger'
    sentence = ''
    errors = ''

    sentence = I18n.t('errors.messages.not_saved', :count => resource.errors.count, :resource => resource.class.model_name.human.downcase) unless resource.nil?

    unless flash.empty?
      if flash[:error]
        flash_alerts << flash[:error]
        sentence = flash[:error]
      end

      if flash[:alert]
        flash_alerts << flash[:alert]
        sentence = flash[:alert]
      end

      if flash[:notice]
        flash_alerts << flash[:notice]
        sentence = flash[:notice]
        type = 'success'
      end
    end

    if resource.nil?
    	return if flash_alerts.empty?

    	errors = flash_alerts
    else
    	return if resource.errors.empty?

    	errors = resource.errors.full_messages
    end

    messages = errors.map { |msg| content_tag(:li, msg) }.join

    alert =    ''
    alert =    "<div class=\"col-md-12\">" unless is_flash
    alert +=     "<div id=\"error_explanation\" class=\"alert alert-#{type} alert-dismissible #{is_flash ? 'flash' : ''}\">"
    alert +=       "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
    alert +=       "<strong>#{sentence}</strong>"
    alert +=       "<ul>#{messages}</ul>" if !resource.nil? && !resource.errors.empty?
    alert +=     "</div>"
    alert +=   "</div>" unless is_flash

    alert.html_safe
	end

	def content_header
		return "#{params[:controller].gsub('/', ' ').humanize.titleize}<small>#{params[:action].humanize.capitalize}</small>".html_safe
	end

  def level_bg(level)
    case level
      when 'high'
        return 'green'
      when 'medium'
        return 'yellow'
      when 'low'
        return 'orange'
      when 'out_of_stock'
        return 'red'
    end
  end

  def account_color(type=nil)
    type = current_user.user_type if type.nil?

    return 'red' if type.eql?('root_admin')
    return 'yellow' if type.eql?('admin')
    return 'aqua' if type.eql?('staff')
  end

  def current_user_type
    return 'Root Administrator' if current_user.root_admin_user_type?
    return 'Administrator' if current_user.admin_user_type?
    return 'Staff' if current_user.staff_user_type?
  end

  def accounts_count
    return User.admins.count + User.staffs.count
  end

  def administrators_count
    return User.admins.count
  end

  def staffs_count
    return User.staffs.count
  end

  def teams_count
    return Team.all.count
  end

  def channels_count
    return Channel.all.count
  end

  def stores_count
    return Store.all.count
  end

  def brands_count
    return Brand.all.count
  end

  def products_count
    return Product.all.count
  end

  def indent
    return '&nbsp;&nbsp;&nbsp;&nbsp;'.html_safe
  end
end
