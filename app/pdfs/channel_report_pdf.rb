class ChannelReportPdf < ApplicationPdf
	def initialize(channel, stores)
    @channel = channel
		@stores = stores

		super(:top_margin => 70)

		header
    move_down(30)
		body
  end

  def header
    text "#{@channel.id} Report", :size => 20
    text "Generated on: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
    move_down(20)

    header_data = [
      ["Channel", "#{@channel.name}"],
      ["Stores", "#{pluralize(@channel.stores.count, 'Store')}"]
    ]

    header_data << ["Channel Image", {:image => "#{@channel.avatar.path(:thumb)}"}] if @channel.avatar.exists?

    table(header_data) do |t|
      t.cells.border_width = 0
      t.position = :center
      t.before_rendering_page { |page| page.column(0).style(:align => :right, :font_style => :bold) }
    end
  end

  def body
    body_data = [
      ["Image", "Store", "Location", "Teams", "Products"]
    ]

    @stores.each do |store|
      data = []
      data << store.avatar.exists? ? {:image => store.avatar.path(:thumb)} : 'N/A'
      data << store.name
      data << store.location
      data << pluralize(store.teams.count, 'Team')
      data << pluralize(store.products.count, 'Product')

      body_data << data
    end

    table(body_data) { |t| t.position = :center }
  end
end