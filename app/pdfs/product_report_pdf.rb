class ProductReportPdf < ApplicationPdf
	def initialize(product, sdate, edate)
		@product = product
		@sdate = sdate
		@edate = edate
		@reports = @product.reports.generated(@sdate, @edate)

		super(:top_margin => 70)

		header
    move_down(30)
		body
  end

  def header
    text "#{@product.id} Report", :size => 20
    text "Generated on: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
    move_down(20)

    header_data = [
      ["Product", "#{@product.name}"],
      ["Brand", "#{@product.brand_name}"]
    ]

    header_data << ["Product Image", {:image => "#{@product.avatar.path(:thumb)}"}] if @product.avatar.exists?

    header_data << ["Report dates", "#{@sdate} to #{@edate}"]

    table(header_data) do |t|
      t.cells.border_width = 0
      t.position = :center
      t.before_rendering_page { |page| page.column(0).style(:align => :right, :font_style => :bold) }
    end
  end

  def body
    body_data = [
      ["Date", "Image", "Store", "Channel", "Inventory Level", "Remarks"]
    ]

    @reports.each do |report|
      body_data << [
        report.created_at.strftime("%Y/%m/%d"),
        {:image => report.avatar.path(:thumb)},
        report.store.name, 
        report.store.channel.name, 
        report.inventory_level.humanize.upcase, report.remarks
      ]
    end

    table(body_data) { |t| t.position = :center }
  end
end