class BrandReportPdf < ApplicationPdf
	def initialize(brand, products)
    @brand = brand
		@products = products

		super(:top_margin => 70)

		header
    move_down(30)
		body
  end

  def header
    text "#{@brand.id} Report", :size => 20
    text "Generated on: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
    move_down(20)

    header_data = [
      ["Brand", "#{@brand.name}"],
      ["Products", "#{pluralize(@brand.products.count, 'Product')}"]
    ]

    header_data << ["Brand Image", {:image => "#{@brand.avatar.path(:thumb)}"}] if @brand.avatar.exists?

    table(header_data) do |t|
      t.cells.border_width = 0
      t.position = :center
      t.before_rendering_page { |page| page.column(0).style(:align => :right, :font_style => :bold) }
    end
  end

  def body
    body_data = [
      ["Product", "Code", "Stores"]
    ]

    @products.each do |product|
      data = []

      data << product.name
      data << product.id
      data << pluralize(product.stores.count, 'Store')

      body_data << data
    end

    table(body_data) { |t| t.position = :center }
  end
end