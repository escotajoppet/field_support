class GeneralReportPdf < ApplicationPdf
	def initialize(product, spis)
    @product = product
		@spis = spis

		super(:top_margin => 70)

		header
    move_down(30)
		body
  end

  def header
    text "#{@product.id} Report", :size => 20
    text "Generated on: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
    move_down(20)

    header_data = [
      ["Product", "#{@product.name}"],
      ["Brand", "#{@product.brand_name}"]
    ]

    header_data << ["Product Image", {:image => "#{@product.avatar.path(:thumb)}"}] if @product.avatar.exists?

    table(header_data) do |t|
      t.cells.border_width = 0
      t.position = :center
      t.before_rendering_page { |page| page.column(0).style(:align => :right, :font_style => :bold) }
    end
  end

  def body
    body_data = [
      ["Channel", "Store", "Inventory Level", "Updated On"]
    ]

    @spis.each do |spi|
      body_data << [
        spi.store.channel.name, 
        spi.store.name, 
        spi.inventory_level.humanize.upcase,
        spi.updated_at.strftime("%Y/%m/%d %H:%M:%S")
      ]
    end

    table(body_data) { |t| t.position = :center }
  end
end