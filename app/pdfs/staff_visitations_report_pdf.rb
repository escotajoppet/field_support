class StaffVisitationsReportPdf < ApplicationPdf
	def initialize(staff, reports)
    @staff = staff
		@reports = reports

		super(:top_margin => 70)

		header
    move_down(30)
		body
  end

  def header
    text "#{@staff.id} Report", :size => 20
    text "Generated on: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
    move_down(20)

    header_data = [
      ["Staff Name", "#{@staff.name}"],
      ["Team", "#{@staff.team.name}"]
    ]

    header_data << ["Avatar", {:image => "#{@staff.avatar.path(:thumb)}"}] if @staff.avatar.exists?

    table(header_data) do |t|
      t.cells.border_width = 0
      t.position = :center
      t.before_rendering_page { |page| page.column(0).style(:align => :right, :font_style => :bold) }
    end
  end

  def body
    body_data = [
      ["Date", "Photo", "Store", "Product", "Inventory Level", "Remarks"]
    ]

    @reports.each do |report|
      body_data << [
        report.created_at.strftime("%Y/%m/%d"),
        {:image => report.avatar.path(:thumb)},
        report.store.name, 
        report.product.name, 
        report.inventory_level.humanize.upcase,
        report.remarks
      ]
    end

    table(body_data) { |t| t.position = :center }
  end
end